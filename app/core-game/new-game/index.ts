import { createNewGame } from '../../models/game'

/**
 *
 * Will create an initial game state.
 *
 * @param {GameSettings} settings - Affects how the game works. If none is given it will default to standard yatzy
 * @param {string[]} players - The names of the participating players
 *
 * @returns {Game} This is expected to be passed in to the other core game functions. This is the current state of the game
 */
const newGame = createNewGame

export { newGame }
