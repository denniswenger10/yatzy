import { possibleSolutions as _possibleSolutions } from '../../player-functions/possible-solutions'
import { Solution } from '../../models/solution'
import { Game } from '../../models/game'
import { GameSettings } from '../../models/game-settings'

const concatPossibleSolutions = (
  settings: GameSettings,
  dice: number[]
) => (prev: Solution[][], section: Solution[]) => [
  ...prev,
  ..._possibleSolutions(settings, section, dice),
]

/**
 * Get possible solutions for the current player.
 *
 * @param {Game} game - Current game state
 * @param {number[]} dice - Dice to test for solutions
 *
 * @returns Array of every combination in game, where every combination is an array of every possible solution sorted by score in descending order.
 */
const possibleSolutions = (
  { settings, currentPlayerIndex, players }: Game,
  dice: number[]
) =>
  [
    players[currentPlayerIndex].topSection,
    players[currentPlayerIndex].bottomSection,
  ].reduce(concatPossibleSolutions(settings, dice), [])

export { possibleSolutions }

/* 
const solutions = getSolutions(game, dice)
const nextGame = play(game, solution)

*/
