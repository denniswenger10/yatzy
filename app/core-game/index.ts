import { isGameFinished } from './is-game-finished'
import { newGame } from './new-game'
import { play } from './play'
import { possibleSolutions } from './possible-solutions'

const core = {
  isGameFinished,
  newGame,
  play,
  possibleSolutions,
}

export { core }
