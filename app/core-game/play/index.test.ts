import { play } from './index'
import { createNewGame, createGame } from '../../models/game'
import { createPlayer } from '../../models/player'
import { createGameSettings } from '../../models/game-settings'
import { createSolution } from '../../models/solution'
import { standardYatzy } from '../../game-settings/standard-yatzy'
import { CombinationType } from '../../app-constants'

describe('Makes a move for a player', () => {
  it('Test 1', () => {
    const playerNames = ['Dennis', 'Sofie']

    const solutionToPlay = {
      combinationType: CombinationType.Ones,
      dice: [1, 1, 1],
      score: 3,
    }

    const gameSettings = createGameSettings({
      ...standardYatzy,
      topSection: [CombinationType.Ones, CombinationType.Twos],
      bottomSection: [],
    })

    const game = createNewGame(gameSettings, playerNames)

    const players = playerNames.map((name, i) =>
      createPlayer({
        name,
        topSection: [
          createSolution(
            i === 0
              ? solutionToPlay
              : {
                  combinationType: CombinationType.Ones,
                }
          ),
          createSolution({
            combinationType: CombinationType.Twos,
          }),
        ],
        bottomSection: [],
      })
    )

    const compareTo = createGame({
      players,
      currentPlayerIndex: 1,
      settings: gameSettings,
    })

    expect(play(game, solutionToPlay)).toEqual(compareTo)
  })
})
