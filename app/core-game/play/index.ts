import { Game } from '../../models/game'
import { Solution } from '../../models/solution'
import { playSolution } from '../../player-functions/play-solution'
import { nextPlayer } from '../../player-functions/next-player'
import { update } from 'ramda'

/**
 * Will add the current solution to the current player and change current player.
 *
 * @param {Game} game - Current game state
 * @param {Solution} solution - Solution to merge
 *
 * @returns A new game state with solution added and current player index updated
 */
const play = (
  { settings, currentPlayerIndex, players }: Game,
  solution: Solution
): Game => ({
  settings,
  players: update(
    currentPlayerIndex,
    playSolution(solution, players[currentPlayerIndex]),
    players
  ),
  currentPlayerIndex: nextPlayer(players.length, currentPlayerIndex),
})

export { play }
