import { Game } from '../../models/game'
import { hasPlaysLeft } from '../../player-functions/has-plays-left'

/**
 * Checks if the game is finished.
 * Meaning, no player is able to make more moves.
 *
 * @param {Game} game - Current game state
 */
const isGameFinished = ({ players }: Game) =>
  players.every((player) => !hasPlaysLeft(player))

export { isGameFinished }
