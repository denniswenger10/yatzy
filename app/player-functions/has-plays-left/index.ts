import { Player } from '../../models/player'
import { Solution } from '../../models/solution'

const isSectionFinished = (solutions: Solution[]) =>
  solutions.every(({ score }) => score !== 0)

const hasPlaysLeft = ({ topSection, bottomSection }: Player) =>
  !isSectionFinished(topSection) && !isSectionFinished(bottomSection)

export { hasPlaysLeft }
