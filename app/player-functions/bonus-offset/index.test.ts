import { bonusOffset } from './index'
import { standardYatzy } from '../../game-settings/standard-yatzy'
import { makeSolutionList } from '../../utils/test-helpers'

const _bonusOffset = bonusOffset(standardYatzy)

const test = (scores: number[], toEqual: number) => {
  const solutions = makeSolutionList(
    scores.map((score) => ({ score }))
  )

  expect(_bonusOffset(solutions)).toBe(toEqual)
}

describe('Bonus offset is correct', () => {
  it('should return 23 (63 - 40)', () => {
    test([10, 30], 23)
  })

  it('should return 0 (since sum is greater than bonus threshold)', () => {
    test([10, 30, 30], 0)
  })
})
