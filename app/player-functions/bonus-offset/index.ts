import { GameSettings } from '../../models/game-settings'
import { Solution } from '../../models/solution'
import { sumMap } from '../../utils/sum'
import { curry } from 'ramda'

const bonusOffset = curry(
  (gameSettings: GameSettings, topSection: Solution[]) => {
    const sumOfSection = sumMap(({ score }) => score, topSection)

    const offset = gameSettings.bonusThreshold - sumOfSection

    return offset < 0 ? 0 : offset
  }
)

export { bonusOffset }
