import { nextPlayer } from './index'

describe('Gets the index of the next player', () => {
  it('Test 1', () => {
    expect(nextPlayer(4, 0)).toBe(1)
  })

  it('Test 2', () => {
    expect(nextPlayer(4, 3)).toBe(0)
  })

  it('Test 3', () => {
    expect(nextPlayer(1, 0)).toBe(0)
  })

  it('Test 4', () => {
    expect(nextPlayer(100, 50)).toBe(51)
  })

  it('Test 5', () => {
    expect(nextPlayer(99, 98)).toBe(0)
  })
})
