import { curry } from 'ramda'

/**
 * Will get the index of the next player.
 */
const nextPlayer = curry(
  (amountOfPlayers: number, currentPlayer: number) =>
    (currentPlayer + 1) % amountOfPlayers
)

export { nextPlayer }
