import { Solution, Player } from '../../models'

const updateSolution = (solution: Solution, solutions: Solution[]) =>
  solutions.map((_sol) =>
    _sol.combinationType === solution.combinationType
      ? solution
      : _sol
  )

//TODO: Write tests
const playSolution = (solution: Solution, player: Player): Player => {
  const topSection = updateSolution(solution, player.topSection)
  const bottomSection = updateSolution(solution, player.bottomSection)

  return {
    ...player,
    topSection,
    bottomSection,
  }
}

export { playSolution }
