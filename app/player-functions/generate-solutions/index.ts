import { Solution, createSolution } from '../../models/solution'
import { CombinationType } from '../../app-constants'

const generateSolutions = (
  combinationTypes: CombinationType[]
): Solution[] =>
  combinationTypes.map((combinationType) =>
    createSolution({
      dice: [],
      score: 0,
      combinationType,
    })
  )

export { generateSolutions }
