import { Solution, GameSettings } from '../../models'
import { getCombinationCollector } from '../../combination-collectors/get-combination-collector'
import { filterMap } from '../../utils'
import { curry } from 'ramda'

const possibleSolutions = curry(
  (
    gameSettings: GameSettings,
    solutionsToTest: Solution[],
    dice: number[]
  ) =>
    filterMap(
      ({ combinationType, score }) =>
        score === 0
          ? getCombinationCollector(combinationType)({
              gameSettings,
              dice,
            })
          : undefined,
      solutionsToTest
    )
)

export { possibleSolutions }
