import { possibleSolutions } from './index'
import { standardYatzy } from '../../game-settings/standard-yatzy'
import { CombinationType } from '../../app-constants'
import { makeSolutionList } from '../../utils/test-helpers'

describe('If solutions returned are correct', () => {
  it('Test 1', () => {
    const solutionsToTest = makeSolutionList([
      {
        combinationType: CombinationType.Chance,
      },
    ])

    const dice = [1, 1, 1, 1, 1]

    expect(
      possibleSolutions(standardYatzy)(solutionsToTest, dice)
    ).toEqual([
      [
        {
          dice: [1, 1, 1, 1, 1],
          score: 5,
          combinationType: CombinationType.Chance,
        },
      ],
    ])
  })

  it('Test 2', () => {
    const solutionsToTest = makeSolutionList([
      {
        score: 3,
        combinationType: CombinationType.Chance,
      },
    ])

    const dice = [1, 1, 1, 1, 1]

    expect(
      possibleSolutions(standardYatzy)(solutionsToTest, dice)
    ).toEqual([])
  })

  it('Test 3', () => {
    const solutionsToTest = makeSolutionList([
      {
        combinationType: CombinationType.TwoPairs,
      },
      {
        combinationType: CombinationType.Chance,
      },
      {
        combinationType: CombinationType.Yatzy,
      },
    ])

    const dice = [1, 1, 1, 1, 1]

    expect(
      possibleSolutions(standardYatzy)(solutionsToTest, dice)
    ).toEqual([
      [],
      [
        {
          dice: [1, 1, 1, 1, 1],
          score: 5,
          combinationType: CombinationType.Chance,
        },
      ],
      [
        {
          dice: [1, 1, 1, 1, 1],
          score: 50,
          combinationType: CombinationType.Yatzy,
        },
      ],
    ])
  })

  it('Test 4', () => {
    const solutionsToTest = makeSolutionList([
      {
        combinationType: CombinationType.TwoPairs,
      },
      {
        combinationType: CombinationType.FullHouse,
      },
    ])

    const dice = [1, 1, 1, 2, 2, 2]

    expect(
      possibleSolutions(standardYatzy)(solutionsToTest, dice)
    ).toEqual([
      [
        {
          dice: [1, 1, 2, 2],
          score: 6,
          combinationType: CombinationType.TwoPairs,
        },
      ],
      [
        {
          dice: [1, 1, 2, 2, 2],
          score: 8,
          combinationType: CombinationType.FullHouse,
        },
        {
          dice: [1, 1, 1, 2, 2],
          score: 7,
          combinationType: CombinationType.FullHouse,
        },
      ],
    ])
  })
})
