enum CombinationType {
  Ones = 'ones',
  Twos = 'twos',
  Threes = 'threes',
  Fours = 'fours',
  Fives = 'fives',
  Sixes = 'sixes',
  OnePair = 'one-pair',
  TwoPairs = 'two-pairs',
  ThreeOfAKind = 'three-of-a-kind',
  FourOfAKind = 'four-of-a-kind',
  SmallStraight = 'small-straight',
  LargeStraight = 'large-straight',
  FullStraight = 'full-straight',
  FullHouse = 'full-house',
  Chance = 'chance',
  Yatzy = 'yatzy',
}

export { CombinationType }
