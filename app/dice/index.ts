import { randomNumber } from '../utils/random-number'

/**
 * Get an array with random numbers.
 * To use standard dice you set min to 1 and max to 6.
 */
const roll = (min: number = 1) => (max: number = 6) => (
  amountOfDice: number = 5
) => Array.from(Array(amountOfDice), () => randomNumber(min, max))

/**
 * Replaces all but the locked indexes with new random numbers.
 * To use standard dice you set min to 1 and max to 6.
 */
const reroll = (min: number = 1) => (max: number = 6) => (
  lockedIndexes: number[] = []
) => (dice: number[]) =>
  dice.map((die, i) =>
    lockedIndexes.includes(i) ? randomNumber(min, max) : die
  )

/**
 * Same as `roll` but min and max set to use a standard die
 */
const rollStandard = roll(1)(6)

/**
 * Same as `reroll` but min and max set to use a standard die
 */
const rerollStandard = reroll(1)(6)

/**
 * Functions for getting random dice and rerolling dice with feature for locking specific dice.
 */
const dice = {
  roll,
  reroll,
  rollStandard,
  rerollStandard,
}

export { dice }
