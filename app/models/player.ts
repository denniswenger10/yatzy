import { Solution } from './solution'
import { generateSolutions } from '../player-functions/generate-solutions'
import { gameSettings } from '../game-settings'

const { standardYatzy } = gameSettings

interface Player {
  name: string
  score: number
  bonus: number
  bonusDifference: number
  topSection: Solution[]
  bottomSection: Solution[]
}

const defaultPlayer = {
  name: 'Player 1',
  score: 0,
  bonus: 0,
  bonusDifference: 63,
  topSection: generateSolutions(standardYatzy.topSection),
  bottomSection: generateSolutions(standardYatzy.bottomSection),
}

const createPlayer = (player: {
  name?: string
  score?: number
  bonus?: number
  bonusDifference?: number
  topSection?: Solution[]
  bottomSection?: Solution[]
}): Player => {
  return { ...defaultPlayer, ...player }
}

export { createPlayer, Player }
