import { GameSettings, Player, createPlayer } from '.'
import { gameSettings } from '../game-settings'
import { generateSolutions } from '../player-functions/generate-solutions'

const { standardYatzy } = gameSettings

interface Game {
  players: Player[]
  settings: GameSettings
  currentPlayerIndex: number
}

const defaultGame = {
  players: createPlayer({
    name: 'Player 1',
    topSection: generateSolutions(standardYatzy.topSection),
    bottomSection: generateSolutions(standardYatzy.bottomSection),
  }),
  settings: standardYatzy,
  currentPlayerIndex: 0,
}

const createNewGame = (
  settings: GameSettings = standardYatzy,
  players: string[]
): Game => ({
  players: players.map((name) =>
    createPlayer({
      name,
      topSection: generateSolutions(settings.topSection),
      bottomSection: generateSolutions(settings.bottomSection),
    })
  ),
  settings,
  currentPlayerIndex: 0,
})

const createGame = ({
  players,
  settings,
  currentPlayerIndex,
}: {
  players: Player[]
  settings: GameSettings
  currentPlayerIndex: number
}): Game => ({
  ...defaultGame,
  players,
  settings,
  currentPlayerIndex,
})

export { createGame, createNewGame, Game }
