interface NewPlayer {
  name: string
  score: number
  bonus: number
  bonusDifference: number
}

const defaultPlayer = {
  name: 'Player 1',
  score: 0,
  bonus: 0,
  bonusDifference: 63,
}

const createNewPlayer = (player: {
  name?: string
  score?: number
  bonus?: number
  bonusDifference?: number
}): NewPlayer => {
  return { ...defaultPlayer, ...player }
}

export { createNewPlayer, NewPlayer }
