import { CombinationType } from '../app-constants'
import { sum as _sum } from '../utils/sum'

interface Solution {
  score: number
  dice: number[]
  combinationType: CombinationType
}

const createSolution = ({
  dice,
  score,
  combinationType,
}: {
  dice?: number[]
  score?: number
  combinationType: CombinationType
}): Solution => {
  dice = dice === undefined ? [] : dice

  return {
    dice,
    score: score ? score : _sum(dice),
    combinationType,
  }
}

export { createSolution, Solution }
