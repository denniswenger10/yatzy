import { CombinationType } from '../app-constants'
import { gameSettings } from '../game-settings'

interface GameSettings {
  yatzyValue: number
  bonusThreshold: number
  bonusValue: number
  topSection: CombinationType[]
  bottomSection: CombinationType[]
  amountOfDices: number
}

const { standardYatzy } = gameSettings

const {
  bonusThreshold,
  bonusValue,
  topSection,
  bottomSection,
  yatzyValue,
  amountOfDices,
} = standardYatzy

const createGameSettings = (gameSettings: {
  bonusThreshold?: number
  bonusValue?: number
  topSection?: CombinationType[]
  bottomSection?: CombinationType[]
  yatzyValue?: number
  amountOfDices?: number
}): GameSettings => {
  return {
    bonusThreshold,
    bonusValue,
    topSection,
    bottomSection,
    yatzyValue,
    amountOfDices,
    ...gameSettings,
  }
}

export { createGameSettings, GameSettings }
