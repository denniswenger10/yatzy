import { standardYatzy } from './standard-yatzy'

/**
 * Game settings to use when creating a new game
 */
const gameSettings = {
  standardYatzy,
}

export { gameSettings }
