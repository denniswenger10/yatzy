import { CombinationType } from '../app-constants'
import { GameSettings } from '../models/game-settings'

const topSection: CombinationType[] = [
  CombinationType.Ones,
  CombinationType.Twos,
  CombinationType.Threes,
  CombinationType.Fours,
  CombinationType.Fives,
  CombinationType.Sixes,
]

const bottomSection: CombinationType[] = [
  CombinationType.OnePair,
  CombinationType.TwoPairs,
  CombinationType.ThreeOfAKind,
  CombinationType.FourOfAKind,
  CombinationType.SmallStraight,
  CombinationType.LargeStraight,
  CombinationType.FullHouse,
  CombinationType.Chance,
  CombinationType.Yatzy,
]

/**
 * This is the standard 5 dice yatzy most will be familiar with.
 */
const standardYatzy: GameSettings = {
  bonusThreshold: 63,
  bonusValue: 50,
  topSection,
  bottomSection,
  yatzyValue: 50,
  amountOfDices: 5,
}

export { standardYatzy }
