import { GameSettings, Solution } from '../models'

interface CombinationCollector {
  ({
    gameSettings,
    dice,
  }: {
    gameSettings: GameSettings
    dice: number[]
  }): Solution[]
}

export { CombinationCollector }
