import { smallStraight } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.SmallStraight

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = smallStraight({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1).toEqual([
    {
      score: 3,
      dice: [1, 2],
      combinationType,
    },
  ])

  const test2 = smallStraight({
    dice: [1, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test2).toEqual([
    {
      score: 15,
      dice: [1, 2, 3, 4, 5],
      combinationType,
    },
  ])

  const test3 = smallStraight({
    dice: [2, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test3).toEqual([])

  const test4 = smallStraight({
    dice: [1, 2, 3, 4, 6, 6],
    gameSettings: standardYatzy,
  })
  expect(test4).toEqual([])

  const test5 = smallStraight({
    dice: [1, 1, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test5).toEqual([])
})
