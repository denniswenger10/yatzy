import { fullHouse } from './index'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.FullHouse

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = fullHouse({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1).toEqual([])

  const test2 = fullHouse({
    dice: [1, 1, 1, 1, 2],
    gameSettings: standardYatzy,
  })
  expect(test2).toEqual([])

  const test3 = fullHouse({
    dice: [5, 5, 5, 5, 5, 5, 5, 5, 4, 4, 5],
    gameSettings: standardYatzy,
  })
  expect(test3).toEqual([
    { score: 23, dice: [4, 4, 5, 5, 5], combinationType },
  ])

  const test4 = fullHouse({
    dice: [1, 1, 1, 2, 2, 2],
    gameSettings: standardYatzy,
  })
  expect(test4).toEqual([
    { score: 8, dice: [1, 1, 2, 2, 2], combinationType },
    { score: 7, dice: [1, 1, 1, 2, 2], combinationType },
  ])
})
