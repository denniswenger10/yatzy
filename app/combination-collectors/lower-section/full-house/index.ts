import { CombinationCollector } from '../../types'
import { createDiceMap } from '../../helpers/create-dice-map'
import { sortSolutions } from '../../helpers/sort-solutions'
import { takeFromDiceMapValues } from '../../helpers/take-from-dice-map-values'
import { allCombinationsBlendMap } from '../../../utils/all-combinations'
import { createSolution } from '../../../models/solution'
import { CombinationType } from '../../../app-constants'

const fullHouse: CombinationCollector = ({ dice }) => {
  const diceMapValues = Object.values(createDiceMap(dice))

  const allPairs = takeFromDiceMapValues(2, diceMapValues)
  const allThreeOfAKind = takeFromDiceMapValues(3, diceMapValues)

  const blendFunction = (first: number[], second: number[]) =>
    first[0] !== second[0]
      ? createSolution({
          dice:
            first[0] > second[0]
              ? [...second, ...first]
              : [...first, ...second],
          combinationType: CombinationType.FullHouse,
        })
      : undefined

  return sortSolutions(
    allCombinationsBlendMap(blendFunction, allPairs, allThreeOfAKind)
  )
}

export { fullHouse }
