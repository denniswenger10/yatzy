import { chance } from './index'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.Chance

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = chance({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1).toEqual([
    {
      score: 4,
      dice: [1, 2, 1],
      combinationType,
    },
  ])

  const test2 = chance({
    dice: [1, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test2).toEqual([
    {
      score: 21,
      dice: [1, 2, 3, 4, 5, 6],
      combinationType,
    },
  ])

  const test3 = chance({
    dice: [2, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test3).toEqual([
    {
      score: 22,
      dice: [2, 2, 3, 4, 5, 6],
      combinationType,
    },
  ])
})
