import { CombinationCollector } from '../..'
import { createSolution } from '../../../models'
import { CombinationType } from '../../../app-constants'

const chance: CombinationCollector = ({ dice }) => [
  createSolution({ dice, combinationType: CombinationType.Chance }),
]

export { chance }
