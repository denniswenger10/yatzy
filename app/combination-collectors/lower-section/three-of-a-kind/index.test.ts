import { threeOfAKind } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.ThreeOfAKind

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = threeOfAKind({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1.length).toBe(0)

  const test2 = threeOfAKind({
    dice: [1, 2, 1, 1, 2],
    gameSettings: standardYatzy,
  })
  expect(test2.length).toBe(1)
  expect(test2).toEqual([
    { score: 3, dice: [1, 1, 1], combinationType },
  ])

  const test3 = threeOfAKind({
    dice: [5, 1, 2, 5, 1, 1, 2, 5],
    gameSettings: standardYatzy,
  })
  expect(test3.length).toBe(2)
  expect(test3).toEqual([
    { score: 15, dice: [5, 5, 5], combinationType },
    { score: 3, dice: [1, 1, 1], combinationType },
  ])
})
