import { CombinationCollector } from '../..'
import { ofAKind } from '../../helpers/of-a-kind'
import { CombinationType } from '../../../app-constants'

const threeOfAKind: CombinationCollector = ({ dice }) =>
  ofAKind(3, dice, CombinationType.ThreeOfAKind)

export { threeOfAKind }
