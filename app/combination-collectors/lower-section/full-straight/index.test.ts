import { fullStraight } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.FullStraight

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = fullStraight({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1).toEqual([])

  const test2 = fullStraight({
    dice: [1, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test2).toEqual([
    {
      score: 21,
      dice: [1, 2, 3, 4, 5, 6],
      combinationType,
    },
  ])

  const test3 = fullStraight({
    dice: [2, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test3).toEqual([])

  const test4 = fullStraight({
    dice: [1, 2, 3, 4, 6, 6],
    gameSettings: standardYatzy,
  })
  expect(test4).toEqual([])

  const test5 = fullStraight({
    dice: [1, 1, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test5).toEqual([])
})
