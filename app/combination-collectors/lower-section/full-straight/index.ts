import { CombinationCollector } from '../..'
import { isInSequence } from '../../helpers/is-in-sequence'
import { createSolution } from '../../../models'
import { sort } from 'ramda'
import { CombinationType } from '../../../app-constants'

const fullStraight: CombinationCollector = ({ dice }) => {
  const Straight = sort((a, b) => a - b, dice)

  return isInSequence(Straight) && Straight[0] === 1
    ? [
        createSolution({
          dice: Straight,
          combinationType: CombinationType.FullStraight,
        }),
      ]
    : []
}

export { fullStraight }
