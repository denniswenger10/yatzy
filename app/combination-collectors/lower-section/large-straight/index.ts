import { CombinationCollector } from '../..'
import { isInSequence } from '../../helpers/is-in-sequence'
import { uniqueAndSorted } from '../../../utils/unique-and-sorted'
import { createSolution } from '../../../models'
import { CombinationType } from '../../../app-constants'

const largeStraight: CombinationCollector = ({ dice }) => {
  const uniqueValues = uniqueAndSorted(dice)

  const Straight =
    uniqueValues.length < dice.length
      ? uniqueValues
      : uniqueValues.slice(1)

  return isInSequence(Straight) &&
    Straight[0] === 2 &&
    Straight.length === dice.length - 1
    ? [
        createSolution({
          dice: Straight,
          combinationType: CombinationType.LargeStraight,
        }),
      ]
    : []
}

export { largeStraight }
