import { largeStraight } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.LargeStraight

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = largeStraight({
    dice: [3, 2, 3],
    gameSettings: standardYatzy,
  })
  expect(test1).toEqual([
    {
      score: 5,
      dice: [2, 3],
      combinationType,
    },
  ])

  const test2 = largeStraight({
    dice: [1, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test2).toEqual([
    {
      score: 20,
      dice: [2, 3, 4, 5, 6],
      combinationType,
    },
  ])

  const test3 = largeStraight({
    dice: [2, 2, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test3).toEqual([
    {
      score: 20,
      dice: [2, 3, 4, 5, 6],
      combinationType,
    },
  ])

  const test4 = largeStraight({
    dice: [1, 2, 3, 4, 6, 6],
    gameSettings: standardYatzy,
  })
  expect(test4).toEqual([])

  const test5 = largeStraight({
    dice: [1, 1, 3, 4, 5, 6],
    gameSettings: standardYatzy,
  })
  expect(test5).toEqual([])

  const test6 = largeStraight({
    dice: [2, 3, 4, 5, 6, 6],
    gameSettings: standardYatzy,
  })
  expect(test6).toEqual([
    {
      score: 20,
      dice: [2, 3, 4, 5, 6],
      combinationType,
    },
  ])
})
