import { onePair } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.OnePair

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = onePair({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1[0].score).toBe(2)
  expect(test1[0].dice).toEqual([1, 1])

  const test2 = onePair({
    dice: [1],
    gameSettings: standardYatzy,
  })
  expect(test2.length).toBe(0)

  const test3 = onePair({
    dice: [1, 2, 1, 2],
    gameSettings: standardYatzy,
  })
  expect(test3.length).toBe(2)
  expect(test3).toEqual([
    { score: 4, dice: [2, 2], combinationType },
    { score: 2, dice: [1, 1], combinationType },
  ])

  const test4 = onePair({
    dice: [1, 2, 1, 1, 1, 1],
    gameSettings: standardYatzy,
  })
  expect(test4[0].score).toBe(2)
  expect(test4[0].dice).toEqual([1, 1])
})
