import { CombinationCollector } from '../..'
import { ofAKind } from '../../helpers/of-a-kind'
import { CombinationType } from '../../../app-constants'

const onePair: CombinationCollector = ({ dice }) =>
  ofAKind(2, dice, CombinationType.OnePair)

export { onePair }
