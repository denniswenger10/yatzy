import { fourOfAKind } from './index'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.FourOfAKind

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = fourOfAKind({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1.length).toBe(0)

  const test2 = fourOfAKind({
    dice: [1, 2, 1, 1, 1],
    gameSettings: standardYatzy,
  })
  expect(test2.length).toBe(1)
  expect(test2).toEqual([
    {
      score: 4,
      dice: [1, 1, 1, 1],
      combinationType,
    },
  ])

  const test3 = fourOfAKind({
    dice: [5, 1, 2, 5, 1, 1, 1, 2, 5, 5],
    gameSettings: standardYatzy,
  })
  expect(test3.length).toBe(2)
  expect(test3).toEqual([
    {
      score: 20,
      dice: [5, 5, 5, 5],
      combinationType,
    },
    {
      score: 4,
      dice: [1, 1, 1, 1],
      combinationType,
    },
  ])
})
