import { CombinationCollector } from '../..'
import { ofAKind } from '../../helpers/of-a-kind'
import { CombinationType } from '../../../app-constants'

const fourOfAKind: CombinationCollector = ({ dice }) =>
  ofAKind(4, dice, CombinationType.FourOfAKind)

export { fourOfAKind }
