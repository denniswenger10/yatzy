import { twoPairs } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.TwoPairs

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = twoPairs({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1.length).toBe(0)

  const test2 = twoPairs({
    dice: [1, 2, 1, 2],
    gameSettings: standardYatzy,
  })
  expect(test2.length).toBe(1)
  expect(test2).toEqual([
    { score: 6, dice: [1, 1, 2, 2], combinationType },
  ])

  const test3 = twoPairs({
    dice: [5, 1, 2, 6, 1, 4, 2, 5],
    gameSettings: standardYatzy,
  })
  expect(test3.length).toBe(3)
  expect(test3).toEqual([
    { score: 14, dice: [2, 2, 5, 5], combinationType },
    { score: 12, dice: [1, 1, 5, 5], combinationType },
    { score: 6, dice: [1, 1, 2, 2], combinationType },
  ])
})
