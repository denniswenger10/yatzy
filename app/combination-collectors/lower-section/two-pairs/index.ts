import { CombinationCollector } from '../..'
import { createDiceMap } from '../../helpers/create-dice-map'
import { sortSolutions } from '../../helpers/sort-solutions'
import { takeFromDiceMapValues } from '../../helpers/take-from-dice-map-values'
import { allCombinationsMap } from '../../../utils/all-combinations'
import { createSolution } from '../../../models/solution'
import { CombinationType } from '../../../app-constants'

const twoPairs: CombinationCollector = ({ dice }) => {
  const allPairs = takeFromDiceMapValues(
    2,
    Object.values(createDiceMap(dice))
  )

  return sortSolutions(
    allCombinationsMap(
      (first, second) =>
        createSolution({
          dice: [...first, ...second],
          combinationType: CombinationType.TwoPairs,
        }),
      allPairs
    )
  )
}

export { twoPairs }
