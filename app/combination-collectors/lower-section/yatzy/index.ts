import { CombinationCollector } from '../..'
import { createSolution } from '../../../models'
import { CombinationType } from '../../../app-constants'

const yatzy: CombinationCollector = ({ gameSettings, dice }) => {
  return dice.every((die) => die === dice[0])
    ? [
        createSolution({
          dice,
          score: gameSettings.yatzyValue,
          combinationType: CombinationType.Yatzy,
        }),
      ]
    : []
}

export { yatzy }
