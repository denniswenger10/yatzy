import { yatzy } from '.'
import { gameSettings } from '../../../game-settings'
import { CombinationType } from '../../../app-constants'

const combinationType = CombinationType.Yatzy

const { standardYatzy } = gameSettings

it('Collect points for provided number', () => {
  const test1 = yatzy({
    dice: [1, 2, 1],
    gameSettings: standardYatzy,
  })
  expect(test1).toEqual([])

  const test2 = yatzy({
    dice: [1, 1, 1, 1, 2],
    gameSettings: standardYatzy,
  })
  expect(test2).toEqual([])

  const test3 = yatzy({
    dice: [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
    gameSettings: standardYatzy,
  })
  expect(test3).toEqual([
    {
      score: 50,
      dice: [5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5],
      combinationType,
    },
  ])

  const test4 = yatzy({
    dice: [1, 1],
    gameSettings: standardYatzy,
  })
  expect(test4).toEqual([
    { score: 50, dice: [1, 1], combinationType },
  ])

  const test5 = yatzy({
    dice: [1, 1],
    gameSettings: { ...standardYatzy, yatzyValue: 100 },
  })
  expect(test5).toEqual([
    { score: 100, dice: [1, 1], combinationType },
  ])
})
