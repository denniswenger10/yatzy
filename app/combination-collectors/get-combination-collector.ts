import { CombinationType } from '../app-constants'
import {
  chance,
  fives,
  fourOfAKind,
  fours,
  fullHouse,
  fullStraight,
  largeStraight,
  onePair,
  ones,
  sixes,
  smallStraight,
  threeOfAKind,
  threes,
  twoPairs,
  twos,
  yatzy,
  CombinationCollector,
} from '.'

const functionMap = new Map([
  [CombinationType.Chance, chance],
  [CombinationType.Fives, fives],
  [CombinationType.FourOfAKind, fourOfAKind],
  [CombinationType.Fours, fours],
  [CombinationType.FullHouse, fullHouse],
  [CombinationType.FullStraight, fullStraight],
  [CombinationType.LargeStraight, largeStraight],
  [CombinationType.OnePair, onePair],
  [CombinationType.Ones, ones],
  [CombinationType.Sixes, sixes],
  [CombinationType.SmallStraight, smallStraight],
  [CombinationType.ThreeOfAKind, threeOfAKind],
  [CombinationType.Threes, threes],
  [CombinationType.TwoPairs, twoPairs],
  [CombinationType.Twos, twos],
  [CombinationType.Yatzy, yatzy],
])

const dummyCollector: CombinationCollector = ({}) => []

const getCombinationCollector = (combinationType: CombinationType) =>
  functionMap.get(combinationType) || dummyCollector

export { getCombinationCollector }
