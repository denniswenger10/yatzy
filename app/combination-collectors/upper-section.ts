import { CombinationType } from '../app-constants'
import { sumOfNumberInArray } from './helpers/sum-of-number-in-array/index'

const ones = sumOfNumberInArray(1, CombinationType.Ones)
const twos = sumOfNumberInArray(2, CombinationType.Twos)
const threes = sumOfNumberInArray(3, CombinationType.Threes)
const fours = sumOfNumberInArray(4, CombinationType.Fours)
const fives = sumOfNumberInArray(5, CombinationType.Fives)
const sixes = sumOfNumberInArray(6, CombinationType.Sixes)

export { ones, twos, threes, fours, fives, sixes }
