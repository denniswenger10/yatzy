import { filterMap } from '../../../utils'

const takeFromDiceMapValues = (
  amount: number = 2,
  diceMap: number[][]
) =>
  filterMap(
    (x) => (x.length > amount - 1 ? x.slice(0, amount) : undefined),
    diceMap
  )

export { takeFromDiceMapValues }
