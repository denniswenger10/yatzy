import { Solution } from '../../../models'
import { CombinationType } from '../../../app-constants'
import { createSolution } from '../../../models'
import { sortSolutions, createDiceMap } from '..'
import { filterMap } from '../../../utils'

const ofAKind = (
  amount: number = 2,
  dice: number[],
  combinationType: CombinationType
): Solution[] => {
  const filter = (diceArray: number[]) =>
    diceArray.length >= amount
      ? createSolution({
          dice: diceArray.slice(0, amount),
          combinationType,
        })
      : undefined

  return sortSolutions(
    filterMap(filter, Object.values(createDiceMap(dice)))
  )
}

export { ofAKind }
