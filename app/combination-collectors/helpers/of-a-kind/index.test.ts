import { ofAKind } from '.'
import { CombinationType } from '../../../app-constants'

it('Collect points for provided number', () => {
  const test1 = ofAKind(3, [1, 2, 1], CombinationType.ThreeOfAKind)
  expect(test1.length).toBe(0)

  const test2 = ofAKind(
    4,
    [1, 2, 1, 1, 1],
    CombinationType.FourOfAKind
  )
  expect(test2.length).toBe(1)
  expect(test2).toEqual([
    {
      score: 4,
      dice: [1, 1, 1, 1],
      combinationType: CombinationType.FourOfAKind,
    },
  ])

  const test3 = ofAKind(
    4,
    [5, 1, 2, 5, 1, 1, 1, 2, 5, 5],
    CombinationType.FourOfAKind
  )
  expect(test3.length).toBe(2)
  expect(test3).toEqual([
    {
      score: 20,
      dice: [5, 5, 5, 5],
      combinationType: CombinationType.FourOfAKind,
    },
    {
      score: 4,
      dice: [1, 1, 1, 1],
      combinationType: CombinationType.FourOfAKind,
    },
  ])

  const test4 = ofAKind(
    2,
    [1, 2, 1, 1, 1, 1],
    CombinationType.OnePair
  )
  expect(test4[0].score).toBe(2)
  expect(test4[0].dice).toEqual([1, 1])
})
