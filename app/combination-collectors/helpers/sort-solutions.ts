import { Solution } from '../../models/solution'
import { sort } from 'ramda'

const sortSolutions = (solutions: Solution[]): Solution[] =>
  sort((a, b) => b.score - a.score, solutions)

export { sortSolutions }
