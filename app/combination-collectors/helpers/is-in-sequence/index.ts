const isInSequence = (list: number[]) =>
  list.every(
    (num, i) => i === list.length - 1 || num === list[i + 1] - 1
  )

export { isInSequence }
