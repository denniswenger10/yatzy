import { isInSequence } from '.'

it('Collect points for provided number', () => {
  const test1 = isInSequence([1, 2, 1])
  expect(test1).toBe(false)

  const test2 = isInSequence([1, 2, 3, 4, 5])
  expect(test2).toBe(true)

  const test3 = isInSequence([0, 1, 2, 3])
  expect(test3).toBe(true)

  const test4 = isInSequence([100, 101, 102, 103])
  expect(test4).toBe(true)

  const test5 = isInSequence([4, 5, 6, 7, 7])
  expect(test5).toBe(false)

  const test6 = isInSequence([4, 6, 8])
  expect(test6).toBe(false)
})
