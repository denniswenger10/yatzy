const createDiceMap = (dice: number[]) => {
  const diceMap: { [key: number]: number[] } = {}

  dice.forEach((dice) => {
    diceMap[dice]
      ? diceMap[dice].push(dice)
      : (diceMap[dice] = [dice])
  })

  return diceMap
}

export { createDiceMap }
