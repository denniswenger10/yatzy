import { CombinationCollector } from '../../types'
import { createSolution } from '../../../models/solution'
import { CombinationType } from '../../../app-constants'

const sumOfNumberInArray = (
  n: number,
  combinationType: CombinationType
): CombinationCollector => ({ dice }) => {
  const diceWithNumber = dice.filter((die) => die === n)

  return diceWithNumber.length > 0
    ? [createSolution({ dice: diceWithNumber, combinationType })]
    : []
}

export { sumOfNumberInArray }
