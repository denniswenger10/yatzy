import { sumOfNumberInArray } from './index'
import { standardYatzy } from '../../../game-settings/standard-yatzy'
import { CombinationType } from '../../../app-constants'

describe('Collect points for provided number', () => {
  it('Test 1', () => {
    const test = sumOfNumberInArray(
      1,
      CombinationType.OnePair
    )({
      dice: [1],
      gameSettings: standardYatzy,
    })
    expect(test[0].score).toBe(1)
    expect(test[0].dice).toEqual([1])
  })

  it('Test 2', () => {
    const test = sumOfNumberInArray(
      1,
      CombinationType.OnePair
    )({
      dice: [],
      gameSettings: standardYatzy,
    })
    expect(test.length).toBe(0)
  })

  it('Test 3', () => {
    const test = sumOfNumberInArray(
      1,
      CombinationType.OnePair
    )({
      dice: [1, 4, 3, 1, 3, 1],
      gameSettings: standardYatzy,
    })
    expect(test[0].score).toBe(3)
    expect(test[0].dice).toEqual([1, 1, 1])
  })

  it('Test 4', () => {
    const test = sumOfNumberInArray(
      3,
      CombinationType.OnePair
    )({
      dice: [1, 4, 3, 1, 3, 1],
      gameSettings: standardYatzy,
    })
    expect(test[0].score).toBe(6)
    expect(test[0].dice).toEqual([3, 3])
  })
})
