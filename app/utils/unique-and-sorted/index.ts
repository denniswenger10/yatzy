import { sort } from 'ramda'

const uniqueAndSorted = (numbers: number[]) =>
  Array.from(new Set(sort((a, b) => a - b, numbers)))

export { uniqueAndSorted }
