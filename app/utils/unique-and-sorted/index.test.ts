import { uniqueAndSorted } from '.'

it('Collect points for provided number', () => {
  const test1 = uniqueAndSorted([1, 2, 1])
  expect(test1).toEqual([1, 2])

  const test2 = uniqueAndSorted([1, 2, 3, 4, 5, 6])
  expect(test2).toEqual([1, 2, 3, 4, 5, 6])

  const test3 = uniqueAndSorted([2, 2, 3, 4, 5, 6])
  expect(test3).toEqual([2, 3, 4, 5, 6])

  const test4 = uniqueAndSorted([1, 2, 3, 4, 6, 6])
  expect(test4).toEqual([1, 2, 3, 4, 6])

  const test5 = uniqueAndSorted([1, 1, 3, 4, 5, 6])
  expect(test5).toEqual([1, 3, 4, 5, 6])
})
