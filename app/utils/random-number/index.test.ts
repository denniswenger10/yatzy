import { randomNumber } from '.'

const testHelper = (min: number, max: number) => {
  for (let i = 0; i < 100; i++) {
    const test1 = randomNumber(min, max)
    expect(test1 >= min && test1 <= max).toBe(true)
  }
}

it('Test that it gets a random number from min to max, inclusive', () => {
  testHelper(45, 46)
  testHelper(10, 16)
  testHelper(1, 6)
})
