const randomNumber = (min: number = 1, max: number = 6) =>
  Math.floor(Math.random() * (max - min + 1)) + min

export { randomNumber }
