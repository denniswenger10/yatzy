import { filterMap } from '.'

it('Act as a map but filter if nothing is returned', () => {
  const test1 = filterMap(
    (val) => {
      if (val > 1) {
        return val + val
      }
    },
    [1, 2, 3]
  )
  expect(test1).toEqual([4, 6])

  const test2 = filterMap((val) => {
    return val + '...'
  }, [])
  expect(test2).toEqual([])

  const test3 = filterMap(
    (val) => {
      if (val.name !== 'dennis') {
        val.name = val.name.toUpperCase()
        return val
      }
    },
    [{ name: 'dennis' }, { name: 'sofie' }, { name: 'joakim' }]
  )
  expect(test3).toEqual([{ name: 'SOFIE' }, { name: 'JOAKIM' }])
})
