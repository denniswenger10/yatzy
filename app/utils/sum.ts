const sum = (numbers: number[]) =>
  numbers.reduce((prev, curr) => prev + curr, 0)

const sumMap = <T>(adder: (value: T) => number, list: T[]) =>
  list.reduce((prev, curr) => prev + adder(curr), 0)

export { sum, sumMap }
