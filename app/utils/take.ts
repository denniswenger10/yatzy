const take = <T>(amount: number, list: T[]) => list.slice(0, amount)

function* lazyTake<T>(amount: number, list: T[]) {
  let index = 0

  while (index < amount && index != list.length) {
    yield list[index]
    index++
  }
}

export { take, lazyTake }
