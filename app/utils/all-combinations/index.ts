/**
 * Will take an array and make a combination of all elements.
 * If nothing is returned in function it will simply be filtered out in result.
 *
 * @example
 * allCombinationsBlendMap(fn, [1,2,3,4])
 * // returns [fn(1,2), fn(3,4)]
 */
const allCombinationsMap = <T, K>(
  fn: (first: T, second: T) => K,
  list: T[]
): K[] => {
  const results: K[] = []

  for (let i = 0; i < list.length - 1; i++) {
    for (let j = i + 1; j < list.length; j++) {
      const item = fn(list[i], list[j])
      if (item !== undefined) {
        results.push(item)
      }
    }
  }

  return results
}

/**
 * Will take two arrays and make a combination of elements from the two
 * If nothing is returned in function it will simply be filtered out in result.
 *
 * @example
 * allCombinationsBlendMap(fn, [1,2], [3,4])
 * // returns [fn(1,3), fn(2,4)]
 */
const allCombinationsBlendMap = <T, K>(
  fn: (first: T, second: T) => K | undefined,
  firstList: T[],
  secondList: T[]
): K[] => {
  const results: K[] = []

  for (let i = 0; i < firstList.length; i++) {
    for (let j = 0; j < secondList.length; j++) {
      const item = fn(firstList[i], secondList[j])
      if (item !== undefined) {
        results.push(item)
      }
    }
  }

  return results
}

/**
 * Will take an array and make a combination of all elements
 *
 * @example
 * allCombinations([1,2,3,4])
 * // returns [[1,2], [3,4]]
 */
const allCombinations = <T>(list: T[]): T[][] => {
  return allCombinationsMap((first, second) => [first, second], list)
}

export {
  allCombinations,
  allCombinationsMap,
  allCombinationsBlendMap,
}
