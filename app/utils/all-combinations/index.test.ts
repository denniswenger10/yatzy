import { allCombinations, allCombinationsMap } from '.'

it('Makes all combinations of elements in array', () => {
  const test1 = allCombinations([1, 2, 3, 4])
  expect(test1).toEqual([
    [1, 2],
    [1, 3],
    [1, 4],
    [2, 3],
    [2, 4],
    [3, 4],
  ])

  const test2 = allCombinations([1, 2, 3])
  expect(test2).toEqual([
    [1, 2],
    [1, 3],
    [2, 3],
  ])

  const test3 = allCombinationsMap(
    (first, second) => [first + first, second + second],
    [1, 2, 3]
  )
  expect(test3).toEqual([
    [2, 4],
    [2, 6],
    [4, 6],
  ])
})
