import { CombinationType } from '../app-constants'
import { createSolution } from '../models/solution'

const makeSolutionList = (
  solutions: {
    dice?: number[]
    score?: number
    combinationType?: CombinationType
  }[]
) =>
  solutions
    .map((x) => ({
      ...{
        dice: [],
        score: 0,
        combinationType: CombinationType.Chance,
      },
      ...x,
    }))
    .map(createSolution)

export { makeSolutionList }
